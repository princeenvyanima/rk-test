# на разработчика плагинов/интеграторов к ONLYOFFICE Document Server

1) Установить ONLYOFFICE Document Server (https://helpcenter.onlyoffice.com/server/windows/document/install-office-apps.aspx) на локальный сервер (можно и на линукс).

    *использовался сервер для разработчиков (https://helpcenter.onlyoffice.com/installation/docs-developer-docker-compose.aspx)
2) Развернуть тестовый пример (https://api.onlyoffice.com/editors/demopreview) подключения Doc Server на любом из доступных языков и подключиться к своему серверу из шага 1) и открыть файл на редактирование. 
3) Подменить логотип на главной странице в тестовом примере на любую картинку. Сделать скриншот.
![home](screenshots/1.jpg)
4) Подменить логотип в редакторе. Сделать скриншот.

![editor](screenshots/2.jpg)

